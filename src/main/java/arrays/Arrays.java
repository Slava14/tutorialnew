package arrays;

public class Arrays {
    //1
    public static int min(int a, int b) {
        int res;
        if (a > b) {
            res = b;
        } else {
            res = a;
        }
        return res;
    }

    //  битовый операции, двоичная система счисления
    public static boolean isOdd(int x) {
        return (x & 1) == 1;
    }

    //2
    public static int max(int a, int b) {
        int res;
        if (a > b) {
            res = a;
        } else {
            res = b;
        }
        return res;
    }

    //    public static int minN(int[] a) {
    public static int minN(int... a) {
        int res = a[0];
        for (int i = 0; i < a.length; i++) {
            if (a[i] < res) {
                res = a[i];
            }
        }
        return res;
    }

    public static int maxN(int... a) {
        int res = a[0];
        for (int i = 0; i < a.length; i++) {
            if (a[i] > res) {
                res = a[i];
            }
        }
        return res;
    }

    public static int sumOfValues(int... a) {
        int res = 0;
        for (int i = 0; i < a.length; i++) {
            res += a[i];
        }
        return res;
    }

    public static int sumAtOdd(int... a) {
        int res = 0;
        for (int i = 0; i < a.length; i++) {
            if (isOdd(i)) {
                res += a[i];
            }
        }

        return res;
    }

    public static int sumOdd(int... a) {
        int res = 0;
        for (int i = 0; i < a.length; i++) {
            if (isOdd(a[i])) {
                res += a[i];
            }
        }

        return res;
    }


    public static int averageValues(int... a) {
        int res = 0;
        res = sumOfValues(a) / a.length;

        return res;
    }

    public static int HowIndValues(int... a) {
        int res = 0;
        for (int i = 0; i < a.length; i++) {
            res = a.length;
        }
        return res;
    }

    /*  a- это все коробки
        a.length - кол-во коробок
        i - номер коробки, начинаются с 0
        a[i] - мы берем коробку по номеру i, и смотрим что в ней
      */
    //найти кол-во вхождений числа в массив целых чисел
    public static int count(int x, int[] a) {
        int res = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] == x)
                res++;

        }
        return res;
    }


    //найти индексы заданного числа в массиве целых чисел
    public static int[] findIndexes(int x, int[] a) {
        int result = 0;
        int count = count(x, a);
        int[] res = new int[count];
        for (int i = 0; i < a.length && result < count; i++) {
            if (a[i] == x) {
                res[result] = i;
                result++;
            }
        }
        return res;
    }

    //проверяющую отсортирован ли массив по возрастаниb
    public static boolean isSorted(int a[]) {
        boolean res = true;

        for (int i = 0; i < a.length - 1; i++)

            if (a[i] > a[i + 1]) {
                res = false;
                break;

            }
        return res;
    }

    //возвращающую массив являющийся поэлементной суммой двух других массивов. Подумать, что делать когда длины массивов не совпадают.

    public static int[] Plus(int[] x, int[] a) {

        int res[] = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            res[i] = a[i] + x[i];
        }
        return res;
    }
}

     /*  int maxx;
                if (a.length >= x.length)
                    maxx = a.length;
                else
                    maxx = x.length;
                int[] res = new int[maxx];*/
