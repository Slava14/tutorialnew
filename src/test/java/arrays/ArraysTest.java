package arrays;

import org.junit.Assert;
import org.junit.Test;

import static arrays.Arrays.*;

public class ArraysTest {

    @Test
    public void testMin() {
        Assert.assertEquals(4, min(4, 7));
        Assert.assertEquals(5, min(9, 5));
    }

    @Test
    public void testMax() {
        Assert.assertEquals(7, max(5, 7));
        Assert.assertEquals(9, max(9, 5));
    }

    @Test
    public void testMinN() {
        Assert.assertEquals(7, minN(9, 7, 12, 67));
        Assert.assertEquals(-7, minN(9, 5, -7, 17));
    }

    @Test
    public void testMaxN() {
        Assert.assertEquals(67, maxN(9, 7, 12, 67));
        Assert.assertEquals(17, maxN(9, 5, -7, 17));
    }

    @Test
    public void testSum() {
        Assert.assertEquals(95, sumOfValues(9, 7, 12, 67));
        Assert.assertEquals(24, sumOfValues(9, 5, -7, 17));
        Assert.assertEquals(5, sumOfValues(5));
    }

    @Test
    public void testSumAtOdd() {
        Assert.assertEquals(0, sumAtOdd(5));
        Assert.assertEquals(3, sumAtOdd(5, 3));
        Assert.assertEquals(4, sumAtOdd(5, 3, 4, 1));
    }

    @Test
    public void testSumOdd() {
        Assert.assertEquals(5, sumOdd(5));
        Assert.assertEquals(8, sumOdd(5, 3));
        Assert.assertEquals(9, sumOdd(5, 3, 4, 1));
    }

    @Test
    public void testIsOdd() {
        Assert.assertTrue(isOdd(5));
        Assert.assertFalse(isOdd(8));
    }

    @Test
    public void testAver() {
        Assert.assertEquals(23, averageValues(9, 7, 12, 67));
        Assert.assertEquals(6, averageValues(9, 5, -7, 17));
    }

    @Test
    public void testInd() {
        Assert.assertEquals(4, HowIndValues(9, 7, 12, 67));
        Assert.assertEquals(4, HowIndValues(9, 5, -7, 17));
    }

    @Test
    public void testCount() {
        Assert.assertEquals(1, count(4, new int[]{1, 2, 4, 6, 3, 2}));
        Assert.assertEquals(3, count(4, new int[]{1, 4, 4, 6, 4, 2}));
        Assert.assertEquals(0, count(4, new int[]{}));
    }

    @Test
    public void testIndNumb() {
        Assert.assertEquals(0, count(4, new int[]{}));
        Assert.assertEquals(0, count(4, new int[]{5}));
        Assert.assertEquals(1, count(4, new int[]{4}));
        Assert.assertEquals(1, count(4, new int[]{1, 2, 4, 6, 3, 2}));
        Assert.assertEquals(3, count(4, new int[]{1, 4, 4, 6, 4, 2}));

    }

    @Test
    public void testFindIndexes() {
        Assert.assertArrayEquals(new int[]{}, findIndexes(4, new int[]{}));
        Assert.assertArrayEquals(new int[]{}, findIndexes(4, new int[]{5}));
        Assert.assertArrayEquals(new int[]{0}, findIndexes(4, new int[]{4}));
        Assert.assertArrayEquals(new int[]{2}, findIndexes(4, new int[]{1, 2, 4, 6, 3, 2}));
        Assert.assertArrayEquals(new int[]{1, 2, 4}, findIndexes(4, new int[]{1, 4, 4, 6, 4, 2}));
    }

    @Test
    public void testSort() {
        Assert.assertEquals(true, Arrays.isSorted(new int[]{}));
        Assert.assertEquals(true, Arrays.isSorted(new int[]{1}));
        Assert.assertEquals(true, Arrays.isSorted(new int[]{1, 2}));
        Assert.assertEquals(false, Arrays.isSorted(new int[]{1, 2, 4, 6, 3, 5}));
        Assert.assertEquals(false, Arrays.isSorted(new int[]{1, 4, 3, 6, 4, 2}));
        Assert.assertEquals(true, Arrays.isSorted(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
        Assert.assertEquals(false, Arrays.isSorted(new int[]{1, 2, 3, 1}));
        Assert.assertEquals(true, Arrays.isSorted(new int[]{1, 2, 2, 3}));

    }

    //возвращающую массив являющийся поэлементной суммой двух других массивов. Подумать, что делать когда длины массивов не совпадают.
    @Test
    public void testPlusArray() {
        Assert.assertArrayEquals(new int[]{}, Arrays.Plus(new int[]{}, new int[]{}));
        Assert.assertArrayEquals(new int[]{4}, Arrays.Plus(new int[]{3}, new int[]{1}));
        Assert.assertArrayEquals(new int[]{3, 7}, Arrays.Plus(new int[]{2, 9}, new int[]{1, -2}));
        Assert.assertArrayEquals(new int[]{5, 1}, Arrays.Plus(new int[]{2, 1}, new int[]{3}));
        Assert.assertArrayEquals(new int[]{4, 3}, Arrays.Plus(new int[]{2}, new int[]{2, 3}));
    }

}